Publishing a new Release
===============================================================================

* [ ] Check that **all** CI jobs run without errors on the `master` branch

* [ ] Close all remaining issues on the current milestone

* [ ] Update the [Changelog](CHANGELOG.md)

* [ ] Write the [announcement](NEWS.md)

* [ ] Upload the zipball to PGXN

* [ ] Check the PGXN install process

* [ ] Close the current milsetone and open the next one

* [ ] Rebase the `stable` branch from `master`

* [ ] Tag the master branch

* [ ] Bump to the new version number in [anon.control]() and [META.json]()

* [ ] Publish the announcement

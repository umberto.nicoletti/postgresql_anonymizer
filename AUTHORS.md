PostgreSQL Anonymizer Development Team
===============================================================================

This is an open project. Feel free to join us and improve this tool. To find out
how you can get involved, please read [CONTRIBUTING.md].


[CONTRIBUTING.md]: CONTRIBUTING.md

Maintainer
-------------------------------------------------------------------------------

* Damien Clochard (@daamien)


Contributors
-------------------------------------------------------------------------------

* Damien Cazeils (www.damiencazeils.com) : Logo
* Ioseph Kim (@i0seph) : Documentation 
* Matiss Zarkevics (@leovingi) : Tests on Amazon RDS
* Peter Goodwin (@Postgressor) : Tests
* Tim (@roconda) : Documentation
* Michał Lipka (@michallipka) : Tests and typos
* Thibaut Madeleine (@madtibo) : original idea :-)
